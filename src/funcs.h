#ifndef STEPIC_ALGORITHMS_AND_DS_FUNCS_H
#define STEPIC_ALGORITHMS_AND_DS_FUNCS_H

#include <vector>

std::vector<int> prime_factors(int n);

std::pair<int, int>
find_indexes_of_max_sum(const std::vector<int>& a, const std::vector<int>& b);

#endif //STEPIC_ALGORITHMS_AND_DS_FUNCS_H
