#include <cmath>
#include <map>
#include "funcs.h"

std::vector<int> prime_factors(int n) {
    if (n == 1) {
        return std::vector<int>{1};
    } else if (n < 1) {
        return std::vector<int>{};
    }

    std::vector<int> primes;

    while (n % 2 == 0) {
        primes.push_back(2);
        n /= 2;
    }

    const int sqrtOfN = static_cast<int>(floor(sqrt(n)));
    for (int i = 3; i <= sqrtOfN; i += 2) {
        while (n % i == 0) {
            primes.push_back(i);
            n /= i;
        }
    }

    if (n > 2) {
        primes.push_back(n);
    }

    return primes;
}

std::pair<int, int>
find_indexes_of_max_sum(const std::vector<int> &a, const std::vector<int> &b) {
    const int size = static_cast<int>(a.size());
    int iMax = 0;
    int jMax = 0;
    int max = a[iMax] + b[jMax];
    int maxInA = a[0];
    int maxInAIndex = 0;
    std::map<int, int> indexToMaxValueIndex;

    for (int i = 1; i < size; ++i) {
        if (a[i] > maxInA) {
            maxInA = a[i];
            maxInAIndex = i;
        }

        indexToMaxValueIndex.insert(std::make_pair(i, maxInAIndex));
    }

    for (int i = 1; i < size; ++i) {
        int maxValueIndex = indexToMaxValueIndex[i];

        const int sum = a[maxValueIndex] + b[i];
        if (sum > max) {
            max = sum;
            iMax = maxValueIndex;
            jMax = i;
        }
    }

    return std::make_pair(iMax, jMax);
}
