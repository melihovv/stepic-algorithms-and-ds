#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/funcs.h"

class TestFindIndexesOfMaxSum : public testing::Test {
protected:
    static const std::vector<std::pair<std::pair<std::vector<int>, std::vector<int>>, std::pair<int, int>>> input_;
};

const std::vector<std::pair<std::pair<std::vector<int>, std::vector<int>>, std::pair<int, int>>> TestFindIndexesOfMaxSum::input_ = {
    std::make_pair(std::make_pair(std::vector<int>{4, -8, 6, 0}, std::vector<int>{-10, 3, 1, 1}), std::make_pair(0, 1)),
    std::make_pair(std::make_pair(std::vector<int>{0, 0, 1, 1}, std::vector<int>{1, 1, 0, 0}), std::make_pair(0, 0)),
    std::make_pair(std::make_pair(std::vector<int>{-28, 23, 27, 42, -31}, std::vector<int>{53, -90, 18, -52, -77}), std::make_pair(2, 2)),
    std::make_pair(std::make_pair(std::vector<int>{1, 1, 1}, std::vector<int>{1, 1, 10}), std::make_pair(0, 2)),
    std::make_pair(std::make_pair(std::vector<int>{1, 1, 1, 7, 1, 10, 1}, std::vector<int>{1, 1, 10, 1, 7, 1, 1}), std::make_pair(3, 4)),
};

TEST_F(TestFindIndexesOfMaxSum, FindIndexesOfMaxSum) {
    for (auto t : input_) {
        ASSERT_EQ(t.second, find_indexes_of_max_sum(t.first.first, t.first.second));
    }
}
