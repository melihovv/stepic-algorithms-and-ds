#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../src/funcs.h"

class TestPrimeFactors : public testing::Test {
protected:
    static const std::vector<std::tuple<int, std::vector<int>>> input_;
};

const std::vector<std::tuple<int, std::vector<int>>> TestPrimeFactors::input_ = {
    std::make_tuple(1, std::vector<int>{1}),
    std::make_tuple(75, std::vector<int>{3, 5, 5}),
};

TEST_F(TestPrimeFactors, PrimeFactors) {
    for (auto t : input_) {
        ASSERT_EQ(std::get<1>(t), prime_factors(std::get<0>(t)));
    }
}
